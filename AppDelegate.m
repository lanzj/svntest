//
//  AppDelegate.m
//  520SHddd
//
//  Created by Macx on 2017/8/17.
//  Copyright © 2017年 520SH.com. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+LQopenLesstion.h"
#import "SHTabBarVC.h"
#import "SHCYTabBarVC.h"
#import "SHReMeWabSkipVC.h"
#import "SHFSMyOrderDetailVC.h"
#import "IQKeyboardManager.h"
#import "SHLocationManager.h"
#import "JPUSHService.h"
#import "SHShare.h"
#import "AppDelegate+EaseMob.h"
#import "AppDelegate+Parse.h"
#import "SHYHUserOrderDetailVC.h"
#import "SHYHOrderDetailVC.h"
#import "SHYHDriverConst.h"
#import "SHYHSourceDetailVC.h"
#import "SHYHTabBarVC.h"
#import "PTOrderDetailVC.h"
#import "SHRGOrderDetailVC.h"
#import "SHRGWaitGetOrderVC.h"
#import "PTEnterVC.h"
#import "SHReAuditStatusVC.h"
#import "SHYHAlertView.h"
#import <AudioToolbox/AudioToolbox.h>
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#import "SHURLProtocolHook.h";
#endif

@interface AppDelegate ()<UIAlertViewDelegate,AVAudioPlayerDelegate>

@property (nonatomic, strong) NSDictionary *notifcationDict;

@property (nonatomic, strong) UIViewController *baseVC;
@property (nonatomic, assign) BOOL isReception; //用来判断接收推送消息时是处于前台还是后台 默认为前台 no为后台 yes为前台
@property (nonatomic, strong) AVAudioPlayer *player;
@end
#define EaseMobAppKey @"zhongmuyu#520shq"
#define JPUSHKey @"d12e18b58f6284c7243f4611"
#define UMShareKey @"556e9cb267e58e52c100158c"

static NSInteger const xUpgradeTag = 10086;
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   // [NSURLProtocol registerClass:[SHURLProtocolHook class]];
    //[NSURLProtocol registerClass:[NSClassFromString(@"SHURLProtocolCache") class]];
    [self p_upgrade];//判断升级
    [self p_scrollVforIOS11];
    [self p_setKeyBoard];
    self.window = [[UIWindow alloc]initWithFrame:kFullScreen];
    self.window.backgroundColor = kWhiteColor;
    self.window.rootViewController = [[SHTabBarVC alloc]init];
    
    //注册百度地图
   [LOCATION installMapSDK];
    //初始化百度导航SDK
//    [SHLocationManager installNavigationSDK];
    
    //极光推送
    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
#if DEBUG
    [JPUSHService setupWithOption:launchOptions appKey:JPUSHKey
                          channel:@"Publish channel" apsForProduction:NO
            advertisingIdentifier:nil];
#else
    [JPUSHService setupWithOption:launchOptions appKey:JPUSHKey
                          channel:@"Publish channel" apsForProduction:YES
            advertisingIdentifier:nil];
#endif

    ///初始化环信SDK
#warning Init SDK，detail in AppDelegate+EaseMob.m
#warning SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    NSString *apnsCertName = nil;
#if DEBUG
    apnsCertName = @"ios_push_development";
#else
    apnsCertName = @"ios_push_distribution";
#endif
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *appkey = [ud stringForKey:EaseMobAppKey];
    if (!appkey) {
        appkey = EaseMobAppKey;
        [ud setObject:appkey forKey:EaseMobAppKey];
    }
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:appkey
                apnsCertName:apnsCertName
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    
    //钟生大讲堂
    _openLessionFMDB = [[LQFMDBCreatOpenLession alloc]init];
    [self lqopenLesssionGetMsgTimer];
    
    /* 设置友盟appkey */
    [SHShare setUmSocialAppkey:UMShareKey];
    
    //设置userAgent
    [self p_setUserAgent];
    // 显示窗口
    [self.window makeKeyAndVisible];
    self.isReception = YES;
    return YES;
}



- (void)p_setKeyBoard{
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}
//
//- (void)upGrade {
//    //2先获取当前工程项目版本号
//    NSDictionary *infoDic=[[NSBundle mainBundle] infoDictionary];
//    NSString *currentVersion=infoDic[@"CFBundleShortVersionString"];
//
//    //3从网络获取appStore版本号
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        NSError *error;
//        NSData *response = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/cn/lookup?id=%@",@"955056509"]]] returningResponse:nil error:nil];
//        if (response == nil) {
//            NSLog(@"你没有连接网络哦");
//            return;
//        }
//        NSDictionary *appInfoDic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:&error];
//        if (error) {
//            NSLog(@"hsUpdateAppError:%@",error);
//            return;
//        }
//        NSArray *array = appInfoDic[@"results"];
//        NSDictionary *dic = array[0];
//        NSString *appStoreVersion = dic[@"version"];
//        //打印版本号
//        NSLog(@"当前版本号:%@\n商店版本号:%@",currentVersion,appStoreVersion);
//        //4当前版本号小于商店版本号,就更新
//        if([currentVersion compare:appStoreVersion options:NSNumericSearch] == NSOrderedAscending)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                FXAlertView *alertView = [FXAlertView shareAlert];
//                //        kWeakSelf;
//                [alertView showMsg:kUpdateText buttons:@[@"确定"] clickedBlock:^(NSInteger tag) {
//                    NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/m-ishenghuo/id1329942009?mt=8"];
//                    NSURL * url = [NSURL URLWithString:str];
//
//                    [[UIApplication sharedApplication] openURL:url];
//                }];
//            });
//
//        }else{
//            NSLog(@"版本号好像比商店大噢!检测到不需要更新");
//        }
//    });
//}

#pragma mark ******** 升级判断
- (void)p_upgrade{
    
    //2先获取当前工程项目版本号
    NSDictionary *infoDic=[[NSBundle mainBundle] infoDictionary];
    NSString *currentVersion=infoDic[@"CFBundleShortVersionString"];
    
    //3从网络获取appStore版本号
    NSError *error;
    //[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/cn/lookup?id=%@",@"955056509"]]]
    NSData *response = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/cn/lookup?id=%@",@"955056509"]]] returningResponse:nil error:nil];
//    if (response == nil) {
//        NSLog(@"你没有连接网络哦");
//        return;
//    }
    
    if (response != nil) {
        NSDictionary *appInfoDic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            NSLog(@"hsUpdateAppError:%@",error);
            return;
        }
        NSArray *array = appInfoDic[@"results"];
        NSDictionary *dic = array[0];
        NSString *appStoreVersion = dic[@"version"];
        //打印版本号
        NSLog(@"当前版本号:%@\n商店版本号:%@",currentVersion,appStoreVersion);
        //4当前版本号小于商店版本号,就更新
        if([currentVersion compare:appStoreVersion options:NSNumericSearch] == NSOrderedAscending)
        {
            UIAlertView *vc = [[UIAlertView alloc]initWithTitle:nil message:@"发现新版本" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
            vc.tag = xUpgradeTag;
            [vc show];
            
        }else{
            NSLog(@"版本号好像比商店大噢!检测到不需要更新");
        }
    }
   
    /*
    [HttpTool GET:kUpgrade_URL parameters:nil success:^(id responseObject) {
        if (responseObject[@"VersionIOS"]) {
            NSString *lasterVer = responseObject[@"VersionIOS"];
            NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
            NSString *curr_ver = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
            
            int laster = [[lasterVer stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];//最新版本
            int current = [[curr_ver stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];//当前版本
            
            if (current < laster) {
                UIAlertView *vc = [[UIAlertView alloc]initWithTitle:nil message:@"发现新版本" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
                vc.tag = xUpgradeTag;
                [vc show];
            }
        }
    } failure:^(NSError *error) {
        
    }];
     */
}

#pragma mark iOS 11 scrollView设置
- (void)p_scrollVforIOS11{
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        UITableView.appearance.estimatedRowHeight = 0;
        UITableView.appearance.estimatedSectionFooterHeight = 0;
        UITableView.appearance.estimatedSectionHeaderHeight = 0;
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    self.isReception = NO;
   [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    self.isReception = YES;
   [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

//前台时接收推送回调
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler
{
    //判断收到推送是在前台还是在后台
    if (application.applicationState == UIApplicationStateActive) {
        self.isReception = YES;
    }else{
        self.isReception = NO;
    }
    [self dealJPUSHMessageWithNotification:userInfo isForeground:YES];
}

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void (^)())completionHandler
{
    [self dealJPUSHMessageWithNotification:userInfo isForeground:NO];
}

//点击推送栏回调方法
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    self.isReception = NO;
    [self dealJPUSHMessageWithNotification:userInfo isForeground:NO];
}

#pragma mark   ******** 处理推送消息 ********
-(void)dealJPUSHMessageWithNotification:(NSDictionary *)userInfo isForeground:(BOOL)isForeground
{
    self.baseVC = kKeyWindow.topViewController;
    if([[userInfo allKeys]containsObject:@"groupId"]){
        //通知获取消息到数据库 大讲堂
        [kNoteCenter postNotificationName:@"NOTIFICATIONCENTEROFFLINEMSG" object:nil];
        return;
    }else if ([[userInfo allKeys] containsObject:@"status"])
    {
        if([self isYHorPT:userInfo]){
            [self handlerNotificationForFreight:userInfo];
            return;
        }
        //有对应的status 百货 或者 餐饮 的推送消息
        if (isForeground) {  //在前台 则弹出提示框 让用户选择是否打开推送消息
            self.notifcationDict = userInfo;
            NSString *apnCount = userInfo[@"aps"][@"alert"];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示信息" message:apnCount delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"查看", nil];
            [alert show];
            return;
        }
        [self dealKGOrCYJpushMessageWithNotification:userInfo];
        return;
    }

    NSString *apnCount = userInfo[@"aps"][@"alert"];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示信息" message:apnCount delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == xUpgradeTag) {//更新提示框
        if (buttonIndex==1) {
            NSString *str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/m-ishenghuo/id955056509?mt=8"];
            NSURL * url = [NSURL URLWithString:str];
            
            [[UIApplication sharedApplication] openURL:url];
        }
    }else{
        if (buttonIndex == 1) {
            [self dealKGOrCYJpushMessageWithNotification:self.notifcationDict];
        }
    }
}

#pragma mark   ******** 处理 百货 餐饮 分销 的推送 ********
-(void)dealKGOrCYJpushMessageWithNotification:(NSDictionary *)userInfo
{
    if (!LOGINTOOL.isLogin) {
        [self.baseVC presentViewController:[[SHLoginVC alloc]init] animated:YES completion:nil];
        return;
    }
    
    NSInteger msgType = 1;
    if([[userInfo allKeys] containsObject:@"msgType"])
    {
        //1快购  2餐饮 11分销
        msgType = [userInfo[@"msgType"] integerValue];
    }
    
    if (msgType == 1) {//快购
        
        NSString *orderId  = [[userInfo allKeys] containsObject:@"msgId"] ?userInfo[@"msgId"] : userInfo[@"orderId"];
        NSInteger status = [userInfo[@"status"] integerValue];
        
        if (status == 1)
        {    //百货商家订单管理 订单详情
    
            SHReMeWabSkipVC *vc = [[SHReMeWabSkipVC alloc]init];
            vc.URLString = [NSString stringWithFormat:@"%@?orderId=%@&status=1&userName=%@&seq=%@",kKGRequestURL(KG_shopBackOrderDetail_URL),orderId,LOGINTOOL.mobile,LOGINTOOL.SEQ];
            [self.baseVC.navigationController pushViewController:vc animated:YES];

        }else  if (status == 5)
        {      //个人中心消息中心 消息详情
            SHReMeWabSkipVC *vc = [[SHReMeWabSkipVC alloc]init];
            vc.URLString = [NSString stringWithFormat:@"%@?seq=%@&msgId=%@",ME_notificationMEssageDetail_URL,LOGINTOOL.SEQ,orderId];
            [self.baseVC.navigationController pushViewController:vc animated:YES];
        }
        else
        {   //用户 百货订单详情
            SHFSMyOrderDetailVC *vc = [[SHFSMyOrderDetailVC alloc]init];
            vc.orderId = orderId;
            [self.baseVC.navigationController pushViewController:vc animated:YES];
        }
        
    }else if (msgType == 2)//餐饮
    {

        NSInteger status = [userInfo[@"status"] integerValue];
        if (status == 1 || status == 2)
        { // 用户 推给商家 打开餐饮商家后台订单管理
            NSString *storeID = userInfo[@"storeId"];
            SHReMeWabSkipVC *merchantvc = [[SHReMeWabSkipVC alloc]init];
            merchantvc.URLString = [NSString stringWithFormat:@"%@?storeId=%@",Main_Web_CY_ShopBack,storeID];
            [self.baseVC.navigationController pushViewController:merchantvc animated:YES];
            
        }else if (status == 4 || status == 6)
        { // 商家 推给 用户 打开餐饮模块
           SHCYTabBarVC *vc =  [SHCYTabBarVC hf_tabBarVC];
           vc.selectedIndex = 1;
           [self.baseVC presentViewController:vc animated:NO completion:nil];
        }
    }else if (msgType == 3 || msgType == 4){//跑腿/爱运货
       [self handlerNotificationForFreight:userInfo];
        
    }else if (msgType == 11){//分销
        
    }else if (msgType == 12){//代理商后台
        [self.baseVC.navigationController pushViewController:[SHReAuditStatusVC new] animated:YES];
    }
    
}
/*ty
 跑腿运货项目消息格式定义：
 msgType:3 ---3 属于跑腿消息 4---属于运货消息
 orderId：订单号
 status：订单状态
 */
//爱运货/跑腿推送提示处理
- (void)handlerNotificationForFreight:(NSDictionary *)info{
    if (!LOGINTOOL.isLogin) {
        SHLoginVC *loginVC = [[SHLoginVC alloc]init];
        [self.baseVC presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    int msgType = [info[@"msgType"] intValue];
    id orderId = info[@"orderId"];
    if(!orderId || [orderId isKindOfClass:[NSNull class]]) return;
    int stats = [info[@"status"] intValue];
    NSArray *msgs = @[@"好的",@"去看看"];
    if(stats == 1005 || stats == 1009){
        msgs = @[@"好的"];
    }
    
#pragma mark 前台收到推送播放提示音判断
    if(self.isReception) {
        //叮咚声
        [self palyerVoiceFrequency:@"orderRemind.caf"];
        
        //延时播放下个声音
        kWeakSelf(self);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //被投诉
            if (stats == 1001) {
                [weakself palyerVoiceFrequency:@"orderComplaint.caf"];
            }else if (stats == 1006) {
                //新订单可接
                [weakself palyerVoiceFrequency:@"newOrder.caf"];
            }else if (stats == 1010){
                //订单取消
                [weakself palyerVoiceFrequency:@"orderCancel.caf"];
            }
        });
        
    }
    
    __weak typeof(self) wself = self;
    NSString *msg = info[@"aps"][@"alert"];//[SHYHDriverConst msgWithStatus:[info[@"status"] intValue]];
    [[SHYHAlertView shareAlert] showMsg:msg
                                buttons:msgs
                           clickedBlock:^(NSInteger tag) {
                               if(tag == 1){
                                   if(msgType == 4){//运货
                                       [wself jumpToFreightVCWithStatus:stats orderId:orderId];
                                   }else if(msgType == 3){//跑腿-跳转详情
                                       [wself handlerNotificationForPTSendOrder:info];
                                   }
                               }
    }];
}
/*
 推送消息码
 status（司机跑腿）
 1001：被投诉消息
 1002：用户投诉通过（申诉失败）消息
 1003：用户投诉不通过（申诉成功）消息
 1004：注册审核通过
 1005：注册审核不通过  
 1006：新订单可接
 1007：催单
 1008：确认收货
 1009: 封号
 1010:用户取消订单
 status（用户）
 2001：投诉通过消息
 2002：投诉不通过消息
 2003：订单取消
 2004：订单被接
 2005：订单取货通知
 2006：货物送往收货地点
 2007：订单删除
 2008:已送达
 */
- (void)jumpToFreightVCWithStatus:(int)status orderId:(NSString *)orderId{
//    if([wself isFreightDriver]){
        if(status == 1006){ //货源详情
            SHYHSourceDetailVC *sVC = [[SHYHSourceDetailVC alloc] initWithParams:@{@"orderId":orderId}];
             [self.baseVC.navigationController pushViewController:sVC animated:YES];
            
        }else if (status == 1004){
            SHYHTabBarVC *tVC = [[SHYHTabBarVC alloc] initWithType:SHYHTabBarTypeDriver];
            [self.baseVC.navigationController pushViewController:tVC animated:YES];
        }else if(status == 1001 || status == 1002 || status == 1003){ //投诉订单
            SHYHOrderState orderState = SHYHOrderStateReport;//yhv5.1版状态
            SHYHOrderDetailVC *detailVC = [[SHYHOrderDetailVC alloc] initWithParams:@{@"orderId":orderId,@"orderState":@(orderState)}];
            [self.baseVC.navigationController pushViewController:detailVC animated:YES];
        }else if(status == 1007){
            SHYHOrderState orderState = SHYHOrderStateDoing;
            SHYHOrderDetailVC *detailVC = [[SHYHOrderDetailVC alloc] initWithParams:@{@"orderId":orderId,@"orderState":@(orderState)}];
            [self.baseVC.navigationController pushViewController:detailVC animated:YES];
                
        }else if(status == 1008){//yhv5.1版
             SHYHOrderState orderState = SHYHOrderStateDoing;
            SHYHOrderDetailVC *detailVC = [[SHYHOrderDetailVC alloc] initWithParams:@{@"orderId":orderId,@"orderState":@(orderState)}];
            [self.baseVC.navigationController pushViewController:detailVC animated:YES];
            
        }else if(status == 1009){//封号
       
        }else if(status == 1010){//用户取消订单
            SHYHOrderState orderState = SHYHOrderStateCancel;
            SHYHOrderDetailVC *detailVC = [[SHYHOrderDetailVC alloc] initWithParams:@{@"orderId":orderId,@"orderState":@(orderState)}];
            [self.baseVC.navigationController pushViewController:detailVC animated:YES];
        }else{ //用户
        SHYHUserOrderDetailVC *orderDetailVC = [[SHYHUserOrderDetailVC alloc] initWithUserType:SHYHUserType_user orderId:orderId];
        [self.baseVC.navigationController pushViewController:orderDetailVC animated:YES];
    }
}



//1司机 0用户
- (BOOL)isFreightDriver{
    int type = [LOGINTOOL.freightUserType intValue];
    int status = [LOGINTOOL.freightStatus intValue];
    return (type == 1 && (status != 0 || status != 5));
}


- (BOOL)isFreightMsg:(NSDictionary *)info{
    return [info[@"msgType"] intValue] == 4; //msgType == 4为运货
}

- (BOOL)isPTMsg:(NSDictionary *)info{
    return [info[@"msgType"] intValue] == 3; //msgType == 3为跑腿
}

- (BOOL)isYHorPT:(NSDictionary *)info{
    return [self isFreightMsg:info] || [self isPTMsg:info];
}

//跑腿发单推送处理
- (void)handlerNotificationForPTSendOrder:(NSDictionary *)info{
    SHLog(@"跑腿推送:%@",info);
    id orderId = info[@"orderId"];
    if(!orderId || [orderId isKindOfClass:[NSNull class]]) return;
    NSString *status = info[@"status"];
//    kWeakSelf(self);
    if (status.integerValue == 2001 ||
        status.integerValue == 2002 ||
        status.integerValue == 2003 ||
        status.integerValue == 2004 ||
        status.integerValue == 2005 ||
        status.integerValue == 2006 ||
        status.integerValue == 2007 ||
        status.integerValue == 2008){
        /*      {"status":"2004","msgType":"3","orderId":"950574922474921984"}
         status（用户）
         2001：投诉通过消息
         2002：投诉不通过消息
         2003：订单取消
         2004：订单被接
         2005：订单取货通知
         2006: 货物送往收货地点
         2007：订单删除
         2008：已送达
         */
        PTOrderDetailVC *orderDetailVC = [[PTOrderDetailVC alloc]initWithOrderId:orderId];
        [self.baseVC.navigationController pushViewController:orderDetailVC animated:YES];
    } else if (status.integerValue == 1001 ||
               status.integerValue == 1002 ||
               status.integerValue == 1003 ||
               status.integerValue == 1004 ||
               status.integerValue == 1005 ||
               status.integerValue == 1006 ||
               status.integerValue == 1007 ||
               status.integerValue == 1008 ||
               status.integerValue == 1009 ||
               status.integerValue == 1010  ) {
        /*
         1001：被投诉消息
         1002：用户投诉通过（申诉失败）消息
         1003：用户投诉不通过（申诉成功）消息
         1004：注册审核通过
         1005：注册审核不通过
         1006：新订单可接
         1007：催单
         1008：确认收货
         1009: 封号
         1010:用户取消订单
         */
        
        if(status.integerValue == 1006){ //新订单可接
            SHRGWaitGetOrderVC *rgGetOrderVC = [[SHRGWaitGetOrderVC alloc] init];
            rgGetOrderVC.orderId = orderId;
            [self.baseVC.navigationController pushViewController:rgGetOrderVC animated:YES];
            
        } else if (status.integerValue == 1004 || status.integerValue == 1005){//1004注册审核通过;1005注册审核不通过
            PTEnterVC *ptEnterVC = [[PTEnterVC alloc] init];
            [self.baseVC.navigationController pushViewController:ptEnterVC animated:YES];
        } else if(status.integerValue == 1001 || status.integerValue == 1002 || status.integerValue == 1003 || status.integerValue == 1007 || status.integerValue == 1008 || status.integerValue == 1010){ //跳至订单详情
            SHRGOrderDetailVC *rgOrderDetailVC = [[SHRGOrderDetailVC alloc] init];
            rgOrderDetailVC.orderId = orderId;
            [self.baseVC.navigationController pushViewController:rgOrderDetailVC animated:YES];
        } else if (status.integerValue == 1009) {//封号
            
            
        }
    }
}


- (void)p_setUserAgent{
    UIWebView* tempWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString* userAgent = [tempWebView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    NSString *ua = [NSString stringWithFormat:@"%@\\%@",userAgent,@"app_webview"];
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent" : ua, @"User-Agent" : ua}];
}

- (void)palyerVoiceFrequency:(NSString *)name{
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:nil];
    // (2)把音频文件转化成url格式
    NSURL *url = [NSURL fileURLWithPath:path];
    // (3)初始化音频类 并且添加播放文件
    if (self.player != nil) { // 暂时这样 没找到切换url的方法 也没找到释放的方法
        self.player = nil;
        self.player = [[AVAudioPlayer  alloc] initWithContentsOfURL:url error:nil];
    }else{
        self.player = [[AVAudioPlayer  alloc] initWithContentsOfURL:url error:nil];
    }
    
    // (4) 设置初始音量大小 默认1，取值范围 0~1
    self.player.volume = 1;
    // (5)设置音乐播放次数 负数为一直循环，直到stop，0为一次，1为2次，以此类推
    self.player.numberOfLoops = 0;
    self.player.delegate = self;
    // (6)准备播放
    [self.player prepareToPlay];
    // (7)播放
    //暂停背景音乐的播放
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self.player play];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    //播放自己的音频完毕以后 继续背景音乐的播放
    [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags{
    
    // AVAudioSessionInterruptionFlags_ShouldResume 表示被中断的音频可以恢复播放了。
    // 该标识在iOS 6.0 被废除。需要用flags参数，来表示视频的状态。
    if (flags == AVAudioSessionInterruptionFlags_ShouldResume && player != nil){
        [player play];
    }
    
}
@end
